#include <iostream>
#include "lookups.h"
#include "aes_dec.h"
#include "aeskeygen.h"
#include "common.h"

using namespace std;

// declaring methods for decryption
void InvSubBytes(unsigned char*, bool);
void InvShiftRows(unsigned char*, bool);
void InvShiftOnce(unsigned char*, int);
void InvMixColumns(unsigned char*, bool);

// this message takes key length, key, cipher and a boolean attribute v
// if v is true then this method will output the result at each round
// otherwise it will not print anything to stdout
unsigned char* AES_DEC(int keyLen, unsigned char *key, unsigned char *cipher, bool v){

    // length of key in bytes
    int keyLengthBytes = keyLen/8;

    // getRounds function returns the number of rounds depending on the key size
    // as specified in the document
    int numberOfRounds = getRounds(keyLengthBytes);

    // state is 1D array which is programmed to behave like a 2D array
    // to avoid the complexity added in C++ when dealing with multidimensional array
    // required for handling pointers for each array
    auto *state = new unsigned char[16];

    // this method sets up the state using the input
    Setup(state, cipher, v);


    // this methods takes uses the key to generate round keys for each round
    unsigned char* allKeys = keyExpansion(key, keyLengthBytes, v);

    //getting the last round key generated
    unsigned char *roundKey = allKeys + blockLength + blockLength*(numberOfRounds-1);

    // adding the last round key to cipher converted to state matrix
    AddRoundKey(state, roundKey, v);

    // EXP51-CPP. Do not delete an array through a pointer of the incorrect type
    roundKey = nullptr;
    delete roundKey;

    // running rounds for each level
    for(int rounds = getRounds(keyLengthBytes)-2; rounds >= 0; rounds--){

        // this method shifts the each row by the index it is on
        // i.e first row is on zeroth index, it does not get shifted
        // second row is shifted one time
        // third row is shifted two times and fourth, three times
        InvShiftRows(state, v);

        // this method substitutes each byte in state with precomputed inv_sbox values
        InvSubBytes(state, v);

        // moving round key pointer to the desired round key depending on round
        roundKey = allKeys + blockLength + blockLength*rounds;

        // XORing the round key with state, byte by byte
        AddRoundKey(state, roundKey, v);

        // EXP51-CPP. Do not delete an array through a pointer of the incorrect type
        roundKey = nullptr;
        delete roundKey;

        // this method does matrix multiplication of state
        // with matrix [[0e 0b 0d 09],[09 0e 0b 0d],[0d 09 0e 0b], [0b 0d 09 0e]]
        // on Galois Field 2, i.e matrix multiplication of binary bits
        // the XOR'ing instead of addition, which behaved exactly like adding
        // this provides the same effect as inversing the initial mix columns
        // Galois Field 2 multiplication by 02 and 03
        InvMixColumns(state, v);
    }

    // this method shifts the each row by the index it is on
    // i.e first row is on zeroth index, it does not get shifted
    // second row is shifted one time
    // third row is shifted two times and fourth, three times
    InvShiftRows(state, v);

    // this method substitutes each byte in state with precomputed inv_sbox values
    InvSubBytes(state, v);

    // Finally XORing with the original key to get message back
    AddRoundKey(state, key, v);

    // converting from state back to output format
    auto *output = new unsigned char[16];
    Setup(output, state, v);

    // EXP51-CPP. Do not delete an array through a pointer of the incorrect type
    state = nullptr;
    delete state;

    return output;
}

// this method right shifts each row its index times
void InvShiftRows(unsigned char *state, bool v){
    // CTR50-CPP. Guarantee that container indices and iterators are within the valid range
    // CTR52-CPP. Guarantee that library functions do not overflow
    // CTR55-CPP. Do not use an additive operator on an iterator if the result would overflow
    for(int i = 0; i < 4; i++){
        for(int j = 0; j < i; j++){
            InvShiftOnce(state, i);
        }
    }
    display(state, v);
}

// this method shifts the given row one time
void InvShiftOnce(unsigned char *state, int row){
    unsigned char temp = state[4*row+3];
    // CTR50-CPP. Guarantee that container indices and iterators are within the valid range
    // CTR52-CPP. Guarantee that library functions do not overflow
    // CTR55-CPP. Do not use an additive operator on an iterator if the result would overflow
    for(int i = 4*row+3; i > 4*row; i--){
        state[i] = state[i-1];
    }
    state[4*row] = temp;
}

// this method does matrix multiplication of state
// with matrix [[0e 0b 0d 09],[09 0e 0b 0d],[0d 09 0e 0b], [0b 0d 09 0e]]
// on Galois Field 2, i.e matrix multiplication of binary bits
// the XOR'ing instead of addition, which behaved exactly like adding
// this provides the same effect as inversing the initial mix columns
// Galois Field 2 multiplication by 02 and 03
void InvMixColumns(unsigned char *state, bool v) {
    auto newState = new unsigned char[16];
    // CTR50-CPP. Guarantee that container indices and iterators are within the valid range
    // CTR52-CPP. Guarantee that library functions do not overflow
    // CTR55-CPP. Do not use an additive operator on an iterator if the result would overflow
    for(int i = 0; i < 16; i++){
        int k = i/4;
        int j = i%4;
        if(k == 0)
            newState[i] = GaloisFieldMul(state[j], 0x0e) ^ GaloisFieldMul(state[j+4], 0x0b) ^ GaloisFieldMul(0x0d, state[j+8]) ^ GaloisFieldMul(0x09,state[j+12]);
        if(k == 1)
            newState[i] = GaloisFieldMul(0x09,state[j]) ^ GaloisFieldMul(0x0e,state[j+4]) ^ GaloisFieldMul(0x0b,state[j+8]) ^ GaloisFieldMul(0x0d, state[j+12]);
        if(k == 2)
            newState[i] = GaloisFieldMul(0x0d, state[j]) ^ GaloisFieldMul(0x09,state[j+4]) ^ GaloisFieldMul(0x0e,state[j+8]) ^ GaloisFieldMul(0x0b,state[j+12]);
        if(k == 3)
            newState[i] = GaloisFieldMul(0x0b,state[j]) ^ GaloisFieldMul(0x0d, state[j+4]) ^ GaloisFieldMul(0x09, state[j+8]) ^ GaloisFieldMul(0x0e,state[j+12]);
    }
    for(int i = 0; i < 16; i++)
        state[i] = newState[i];
    display(state, v);
}

// This replaces the value for each byte of state with Rjindael Inverse Sbox value
void InvSubBytes(unsigned char *state, bool v){
    // CTR50-CPP. Guarantee that container indices and iterators are within the valid range
    // CTR52-CPP. Guarantee that library functions do not overflow
    // CTR55-CPP. Do not use an additive operator on an iterator if the result would overflow
    for(int i=0; i< 16; i++){
        state[i] = inv_sbox[state[i]];
    }
    display(state, v);
}