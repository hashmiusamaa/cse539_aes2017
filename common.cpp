#include <iostream>
#include "common.h"
using namespace std;


// SECURE CODING STANDARDS
// DCL60-CPP. Obey the one-definition rule
//          This file was specifically created to follow
//          this rule. No one thing has been redefined anywhere
//          in the entire scope of this project.

// fixing block length as prescribed by AES
int blockLength = 16;

// this takes in the input array and generates state array to make it look
// like a matrix where message is represented as column after column instead of
// row after row, for instance, "abcdefghijklmno" will become
// a e i m
// b f j n
// c g k o
// d h l p
void Setup(unsigned char *state, const unsigned char *input, bool v){
    // CTR50-CPP. Guarantee that container indices and iterators are within the valid range
    // CTR52-CPP. Guarantee that library functions do not overflow
    // CTR55-CPP. Do not use an additive operator on an iterator if the result would overflow
    for(int i = 0; i < 4; i++){
        for(int j = 0; j < 4; j++){
            state[4*i + j] = input[4*j + i];
        }
    }
    display(state, v);
}

// this methods XOR's the state with the provided key, byte by byte
void AddRoundKey(unsigned char *state, const unsigned char *roundKey, bool v){
    if(v) {
        cout << "Round: ";
        // CTR50-CPP. Guarantee that container indices and iterators are within the valid range
        // CTR52-CPP. Guarantee that library functions do not overflow
        for (int i = 0; i < 16; i++) {
            cout << hex << (unsigned int)roundKey[i] << " ";
        }
        cout << endl;
    }
    // CTR50-CPP. Guarantee that container indices and iterators are within the valid range
    // CTR52-CPP. Guarantee that library functions do not overflow
    for (int i=0; i<16; i++){
        int k = i/4;
        int j = i%4;
        state[i]= state[i]^roundKey[j*4+k];
    }
    display(state, v);
}

// this method provides the number of rounds
// as prescribed by the AES document
// for given keyLength
int getRounds(int keyLengthBytes){
    if(keyLengthBytes == 16) return 10;
    if(keyLengthBytes == 24) return 12;
    if(keyLengthBytes == 32) return 14;
    return 0;
}

void display_norm(unsigned char *state, bool v){
    if (v){
        // CTR50-CPP. Guarantee that container indices and iterators are within the valid range
        // CTR52-CPP. Guarantee that library functions do not overflow
        for(int i = 0; i < 16; i++){
            cout << state[i] << " ";
        }
        cout << endl;
    }
}

void display(unsigned char *state, bool v){
    if (v){
        // CTR50-CPP. Guarantee that container indices and iterators are within the valid range
        // CTR52-CPP. Guarantee that library functions do not overflow
        for(int i = 0; i < 16; i++){
            int k = i/4;
            int j = i%4;
            cout << hex << (unsigned int)state[j*4+k] << " ";
        }
        cout << endl;
    }
}

void displayHex(unsigned char* array, unsigned long size){
    // CTR50-CPP. Guarantee that container indices and iterators are within the valid range
    // CTR52-CPP. Guarantee that library functions do not overflow
    for(int i=0; i< size; i++){
        std::cout << std::hex << (unsigned int) array[i] << " ";
    }
    std::cout << std::endl;
}

// https://en.wikipedia.org/wiki/Finite_field_arithmetic#Multiplication
// this method performs finite field multiplication of 2 field
// this method is timing attack proof. It will run for
// the same time irrespective of inputs used.
unsigned char GaloisFieldMul(unsigned char a, unsigned char b){
    unsigned char output = 0;
    // Secure coding standard used here.
    // EXP50-CPP. Do not depend on the order of evaluation for side effects
    // CTR50-CPP. Guarantee that container indices and iterators are within the valid range
    // CTR52-CPP. Guarantee that library functions do not overflow
    for(int i = 0; i < 8; i++){
        auto maskb = -(b & 1);
        auto maskba = maskb & a;
        output ^= maskba;
        auto rightShifta = (a >> 7);
        auto mask = -(rightShifta & 1);
        a = a << 1;
        // 0x11B is the reduction polynomial for 1 byte Galois field(2) multiplication
        // as provided by
        // https://csrc.nist.gov/csrc/media/projects/
        // cryptographic-standards-and-guidelines/documents/aes-development/rijndael-ammended.pdf
        auto maskCut = (0x11B & mask);
        a ^= maskCut;
        b >>= 1;
    }
    return output;
}

// this method generates a randomly times for loop
void randomTimedLoop(bool v){
    long num = 100000;
    long finalNum = random()%num;
    if(finalNum > num/2)
        while(finalNum > 0){
            finalNum--;
            if(!v) cout << finalNum << " ";
        }
    cout << endl;
}