//
// Created by cogneto on 11/25/17.
//

#ifndef CSE539_AES2017_CBC_H
#define CSE539_AES2017_CBC_H

unsigned char* ENC_AES_CBC(int keyLength, unsigned char key[], unsigned char m[], unsigned long mLength, bool verbose);
unsigned char* DEC_AES_CBC(int keyLength, unsigned char key[], unsigned char cipher[], unsigned long cipherLength, bool verbose);

#endif //CSE539_AES2017_CBC_H
