//
// Created by cogneto on 11/25/17.
//

#ifndef CSE539_AES2017_COMMON_H
#define CSE539_AES2017_COMMON_H
void Setup(unsigned char*, const unsigned char*, bool);
void AddRoundKey(unsigned char*, const unsigned char*, bool);
void display(unsigned char*, bool);
void display_norm(unsigned char*, bool);
void displayHex(unsigned char* array, unsigned long size);
int getRounds(int);
void randomTimedLoop(bool);
unsigned char GaloisFieldMul(unsigned char a, unsigned char b);

// block length is fixed for AES
extern int blockLength;
#endif //CSE539_AES2017_COMMON_H
