//
// Created by gaurav on 11/23/17.
//
#include "aeskeygen.h"
#include "lookups.h"
#include "common.h"
#include <iostream>
#include <cstring>
#include <openssl/rand.h>
#include <openssl/err.h>

// subWord function takes a one byte  value and returns the corresponding substitution from S-box

// CTR50-CPP. Guarantee that container indices and iterators are within the valid range
// -- Explanation: We are calling this function ourselves. sbox is 256 size. each byte is unsigned type so can only contain
//    0-255 values. So this will always be within valid range.
unsigned char subWord(unsigned char k){

    return sbox[k];
}


// rotWord takes a 4 byte array and does a cyclic left shift
// CTR50-CPP. Guarantee that container indices and iterators are within the valid range
// CTR52-CPP. Guarantee that library functions do not overflow
// -- Explanation: We are calling this function ourselves from the KeyExpansion function. Only words 4 byte long are passed.
//                 This guarantees indices remain in valid range and there is no overflow.
void rotWord(unsigned char k[]){

    unsigned char temp = k[0];
    k[0] = k[1];
    k[1] = k[2];
    k[2] = k[3];
    k[3] = temp;

}

/*
 * This takes an empty key array. It generates keylength number of random bytes using the openssl function.
 * And then fills up the empty array with them.
 * Throws an error if the openssl function is not able to generate the required number of random bytes e.g in case the system doesn't have
 * enough randomness.
*/
// CTR50-CPP. Guarantee that container indices and iterators are within the valid range
// CTR52-CPP. Guarantee that library functions do not overflow
// -- Explanation: Verifying if RAND_bytes was able to generate random bytes or not.
//    Using try catch to handle exceptions that may arise due to wrong keylength or array length or any other run time exception.
//    In all cases a message about why error may have happened is given
unsigned char* genRandomKey(int keyLength){

    unsigned char* key;
    try {
        key = new unsigned char[keyLength];
        if (!RAND_bytes(key, keyLength)) {
            std::cout << ERR_get_error();
            throw std::runtime_error("Openssl threw an error while generating random bits");
        }
    }catch(const std::exception &e){
        std::cerr << e.what();
    }

    return key;
}


/*
 * This expands an already random key with a number pseudo random bytes as required by AES
 * Generates blocksize*(numberOfRounds+1) number of bytes including the original key
 * This takes as input a randomly generated key, an empty array to fill up, and keyLength in bytes
 */

// CTR50-CPP. Guarantee that container indices and iterators are within the valid range
// CTR52-CPP. Guarantee that library functions do not overflow
// -- Explanation: We are calling KeyExpansion from inside our AES implementation
//    and we have taken care that the key array and keyLength variable in fact match and are correct.
//    So its guaranteed there will be no overflow and indices will remain valid
unsigned char* keyExpansion(const unsigned char key[], int keyLengthBytes, bool verbose){


    if(verbose){
        std::cout << "Key Expansion started: " << std::endl;
    }

    int numberOfRounds = getRounds(keyLengthBytes);
    if(verbose){
        std::cout << "Number of Rounds: " << numberOfRounds << std::endl;
    }

    // CTR50-CPP. Guarantee that container indices and iterators are within the valid range
    // -- Explanation: Allocating the correct amount of space beforehand so that indices in the coming code remain
    //    inside valid range
    auto allKeys = new unsigned char[blockLength*(numberOfRounds+1)];
    // Copy the random key into the first keylength bytes in the empty array
    int i=0;
    while(i<keyLengthBytes){
        allKeys[i]=key[i];
        i++;
    }

    // Get the number of rounds corresponding to keylength. This is needed to know how many keys we will generate

    // Making sure the running time of key expansion doesn't vary for different length keys to guard against timing attacks
    // All key lengths will actually run 16 rounds. However as we will see, the result computed from the extra rounds
    // will not be saved or returned
    int fakeNumberOfRounds =  16;
    // The extra "fake" rounds will be saving their computations in this array
    unsigned char allKeysFake[blockLength*(fakeNumberOfRounds+1)];
    // Initializing the array. Values don't matter.
    for(int j=0; j<blockLength*(fakeNumberOfRounds+1); j++){
        allKeysFake[j] = 0xff;
    }

    while(i < blockLength*(fakeNumberOfRounds+1)){

        // word will contain a 4-byte word we will process in the current loop
        // i points to the start byte of the current word.
        // Once the number of keys needed are generated i will exceed blockLength*(numberOfRounds+1) and else will be executed
        // In else as the values themselves dont matter, word consists of some 4 bytes from allKeysFake.

        // CTR50-CPP. Guarantee that container indices and iterators are within the valid range
        // -- Explanation: The variable i at this time is equal to KeyLength in bytes so this operation is valid.
        unsigned char word[4];
        if(i < blockLength*(numberOfRounds+1)) {
            for (int j = 4; j > 0; j--) {
                word[4-j] = allKeys[i - j];
            }
        }else{
            for (int j = 4; j > 0; j--) {
                word[j-1] = allKeysFake[i-1];
            }
        }

        randomTimedLoop(verbose);

        // Whenever i is divisible by keylength
        if(i%(keyLengthBytes)==0){

            // First do a cyclic left shift for the current word
            rotWord(word);
            if(verbose) {
                std::cout << "After rotWord: ";
                for (unsigned char j : word) {
                    std::cout << std::hex << (unsigned int) j << " ";
                }
                std::cout<<std::endl;
            }

            // Then substitute each byte with its corresponding value from Sbox
            for (unsigned char &j : word) {
                unsigned char temp=subWord(j);
                j =temp;
            }
            if(verbose) {
                std::cout << "After subWord: ";
                for (unsigned char j : word) {
                    std::cout << std::hex << (unsigned int) j << " ";
                }
                std::cout<<std::endl;
            }

            // Xor the first byte of the word with corresponding value from RCon look up table
            // CTR50-CPP. Guarantee that container indices and iterators are within the valid range
            // -- Explanation: As previously noted i is already equal to keyLength in bytes in the first loop.
            //    It only increments after that. So (i/keyLengthBytes) will always be >=1.
            //    The maximum value of (i/keyLengthBytes) depends on maximum value of i.
            //    Since there are 16 rounds for every key length, i max will be 17*16 and so (i/keyLengthBytes) will be
            //    max for keyLength 16, value being = 17. rcon is 256 bytes long. So 1-17 is valid range.
            word[0] = word[0]^rcon[i/(keyLengthBytes)];

            if(verbose) {
                std::cout << "After Rcon: " << std::endl;
                for (unsigned char j : word) {
                    std::cout << std::hex << (unsigned int) j << " ";
                }
                std::cout<<std::endl;
            }

        }

        // If keylength is 256 bit then following steps need to be done also
        // whenever the remainder of i and keylength is 16
        else if(keyLengthBytes == 32 && i%(keyLengthBytes)==4*4){

            // Substitute each byte with value from sbox
            for (unsigned char &j : word) {
                unsigned char temp=subWord(j);
                j =temp;
            }
        }

        // For the extra fake rounds part else will be executed.
        // A computation similar to the actual computation is done on the allKeysFake.
        // Its run for all length keys so the computation itself doesn't matter.

        // CTR50-CPP. Guarantee that container indices and iterators are within the valid range
        // -- Explanation: This condition ensures indices remain valid for allKeys variable.
        if(i < blockLength*(numberOfRounds+1)) {
            // In the end for all key lengths and i
            // Xor each byte of the word with the byte keylength bytes before it from the already generated keys
            for (int j = 0; j < 4; j++) {
                allKeys[i + j] = allKeys[i + j - keyLengthBytes] ^ word[j];
            }
        }else{
            for (int j = 0; j < 4; j++) {
                allKeysFake[i + j] = allKeysFake[i + j - keyLengthBytes] ^ word[j];
            }
        }

        // Increment i to point to the next 4 byte word for the next loop
        i=i+4;
    }

    // allKeysFake is deleted once this function ends as it is a local variable
    return allKeys;

}


/*
 * Function used to test the key and keyExpansion using the test cases from the AES Document
 *
int main(){

    std::string keysize;
    std::cin >> keysize;
    std::cout<< keysize << std::endl;

    int keyLengthBytes;
    if(keysize.compare("128")==0){
        keyLengthBytes=16;
    }else if(keysize.compare("192")==0){
        keyLengthBytes=24;
    }
    else if(keysize.compare("256")==0){
        keyLengthBytes=32;
    }else{
        throw std::invalid_argument("AES only supports 128, 192 or 256 bit keys");
    }

    //unsigned char* k = genRandomKey(keyLengthBytes);
    unsigned char k[] = {0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c};
    //unsigned char k[] = {0x8e, 0x73, 0xb0, 0xf7, 0xda, 0x0e, 0x64, 0x52, 0xc8, 0x10, 0xf3, 0x2b, 0x80, 0x90, 0x79, 0xe5, 0x62, 0xf8, 0xea, 0xd2, 0x52, 0x2c, 0x6b, 0x7b};
    //unsigned char k[] = {0x60, 0x3d, 0xeb, 0x10, 0x15, 0xca, 0x71, 0xbe, 0x2b, 0x73, 0xae, 0xf0, 0x85, 0x7d, 0x77, 0x81, 0x1f, 0x35, 0x2c, 0x07, 0x3b, 0x61, 0x08, 0xd7, 0x2d, 0x98, 0x10, 0xa3, 0x09, 0x14, 0xdf, 0xf4};
    for(int i=0; i<keyLengthBytes;i++)
        std::cout<<std::hex << (unsigned int)k[i];

    int numberOfRounds = getRounds(keyLengthBytes);
    unsigned char allKeys[blockLength*(numberOfRounds+1)];
    keyExpansion(k, allKeys, keyLengthBytes, true);
    std::cout<< "KeyExpansion finished" << std::endl;
    std::cout<<std::endl;
    for(int i=0; i<blockLength*(numberOfRounds+1);i++)
        std::cout<<std::hex << (unsigned int)allKeys[i];
}*/