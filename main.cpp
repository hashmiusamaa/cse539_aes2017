#include <iostream>
#include "aes.h"
#include "aeskeygen.h"
#include "aes_dec.h"

// MSC50-CPP. Do not use std::rand() for generating pseudorandom numbers
// MSC51-CPP. Ensure your random number generator is properly seeded
#include "openssl/rand.h"
#include "common.h"
#include "cbc.h"

using namespace std;

// c++ secure coding standards followed
// DCL50-CPP. Do not define a C-style variadic function
//          function with ... was not required anywhere, all functions had fixed input

// DCL51-CPP. Do not declare or define a reserved identifier
//          we have not defined a predefined reserved word anywhere

// DCL52-CPP. Never qualify a reference type with const or volatile
//          const is only used for pointers sent to a function as parameter

// DCL53-CPP. Do not write syntactically ambiguous declarations
//          no syntactically ambitious declarations were done

// DCL54-CPP. Overload allocation and deallocation functions as a pair in the same scope
//          no allocation and deallocation function was required

// DCL55-CPP. Avoid information leakage when passing a class object across a trust boundary
//          classes were not used, nor any class objects, all work was done using generic types.

// DCL56-CPP. Avoid cycles during initialization of static objects
//          we had no need to create static variables using the same method it was in. Seriously who does that?

// DCL57-CPP. Do not let exceptions escape from destructors or deallocation functions
//          no destructors or deallocator functions were used

// DCL58-CPP. Do not modify the standard namespaces
//          no std namespaces were modified in the code

// DCL59-CPP. Do not define an unnamed namespace in a header file
//          we defined no namespace whatsoever.

// DCL60-CPP. Obey the one-definition rule
//          oh yes.

// EXP51-CPP. Do not delete an array through a pointer of the incorrect type
//          delete was always performed on array of the same type, not even for derived

// EXP52-CPP. Do not rely on side effects in unevaluated operands
//          no unevaluated operands were passed as a function parameter

// EXP53-CPP. Do not read uninitialized memory
//          seriously? who does that?

// EXP54-CPP. Do not access an object outside of its lifetime
//          no objects were made, and no pointers were accessed after delete

// EXP60-CPP. Do not pass a nonstandard-layout type object across execution boundaries
//          we have not done this anywhere.

// EXP62-CPP. Do not access the bits of an object representation that are not part of the object's value representation
//          no such bits were accessed
//          only bits were accessed

// INT50-CPP. Do not cast to an out-of-range enumeration value
//          enumeration was not used.

// CTR50-CPP. Guarantee that container indices and iterators are within the valid range
//          it has been ensured by passing only defined size arrays to functions

// CTR52-CPP. Guarantee that library functions do not overflow
//          it has been ensured that only till a particular length of a message is read

// STR50-CPP. Guarantee that storage for strings has sufficient space for character data and the null terminator
//          Strings have been ditched in favor of unsigned char arrays
//          and special effort has been made to only insert the characters
//          that are within the range of the container

// MSC50-CPP. Do not use std::rand() for generating pseudorandom numbers
//          we have used openssl for pseudo random number generator

// MEM53-CPP. Explicitly construct and destruct objects when manually managing object lifetime
//            we have explicitly deleted the objects that we have created wherever their lifetimes might outlast the function

// MSC51-CPP. Ensure your random number generator is properly seeded
//          openssl takes care of the seed itself

// MSC52-CPP. Value-returning functions must return a value from all exit paths
//          that was ensured in every method

// MSC53-CPP. Do not return from a function declared [[noreturn]]
//          that was also ensured for void functions




void testAES(unsigned char*, int, unsigned char*, int, bool);
void cli();
void testCTR(unsigned char*, unsigned char*, int, bool);
void testCBC();

int main() {
    //cout << hex << (unsigned int) GaloisFieldMul(0x02, 0xff);
    unsigned char key[] = "abcdefghijklmnopijklmnopijklmnop";
    unsigned char message[] = "I am a bad bad bad bad bad bad boy.";
    int messageSize = sizeof(message);
    int keyLength = (sizeof(key) - 1)*8;
    bool verbose = true;
    //testAES(message, messageSize, key, keyLength, verbose);
    testCBC();
}


void testAES(unsigned char *input, int messageSize, unsigned char* key, int keyL, bool v){
    auto *message = new unsigned char[16];
    for(int i = 0; i < messageSize; i++){
        message[i] = input[i];
    }
    // EXP52-CPP. Do not rely on side effects in unevaluated operands
    // CTR50-CPP. Guarantee that container indices and iterators are within the valid range
    int difference = 16 - sizeof(input);
    for(int i = messageSize - 1; i < 16; i++){
        message[i] = static_cast<unsigned char>(difference + 1);
    }
    cout << endl << "key: "; display(key, true);

    // CTR50-CPP. Guarantee that container indices and iterators are within the valid range
    // keylength is being sent that ensures size of key
    // message size is always fixed for AES
    unsigned char *cipher = AES(keyL, key, message, v);
    if (v) cout << endl << endl << "DECRYPTING" << endl;

    // CTR50-CPP. Guarantee that container indices and iterators are within the valid range
    // keylength is being sent that ensures size of key
    // message size is always fixed for AES
    unsigned char *output = AES_DEC(keyL, key, cipher, v);
    display_norm(output, true);
}

void cli(){
    int keyLength;
    unsigned char key[16];
    cout << "#####################################" << endl;
    cout << "Choose keylength(128, 192 or 256): " << endl;

    cin >> keyLength;
    while(cin.fail() || (keyLength != 128 && keyLength != 192 && keyLength != 256)){
        cin.clear();
        cout << "Choose keylength(128, 192 or 256): " << endl;
        cin >> keyLength;
    }
    cout << "Enter key of chosen length: " << endl;
    cin >> key;
    while(cin.fail() || sizeof(key) != (float)keyLength/8){
        cout << "Please enter key of chosen length: " << endl;
        cin >> key;
    }
    bool done = false;

    while(true){
        string input;
        cout << "Please enter message to encrypt: ";
        cin >> input;
        // EXP52-CPP. Do not rely on side effects in unevaluated operands
        if(input.size() > sizeof(key)){
            cout << "Message size longer than key size. Try again.";
            continue;
        }
        char exit;
        cout << "Enter q to exit. Press any other key to continue.";
        cout << (char) getchar();
        if((char)getchar() == 'q'){
            break;
        }
    }
}

void testCBC(){

    std::string message;
    std::cout<< "Input the message to be encrypted: ";
    std::cin>> message;
    std::cout << message.length()<< std::endl;
    std::cout << message<< std::endl;

    // Check message is of correct type and size

    unsigned char m[message.length()];
    for (int i=0; i< message.length();i++){
        m[i]=(unsigned char)message[i];
    }

    char c;
    int keylength;

    keylength = 256;

    int keylengthBytes =keylength/8;

    unsigned char* key = genRandomKey(keylengthBytes);

    unsigned char* cipher = ENC_AES_CBC(keylength, key, m, message.length(), false);
    std::cout<<"After cipher returned" << std::endl;
    unsigned long cipherLength = message.length()+blockLength-(message.length()%blockLength)+blockLength;
    displayHex(cipher, cipherLength);

    unsigned char* decrypted_m = DEC_AES_CBC(keylength, key, cipher,cipherLength, true);

    for(int i=0; i<message.length(); i++)
        std::cout << decrypted_m[i] ;

}